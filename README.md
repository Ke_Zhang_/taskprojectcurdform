This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app). It has unit testing with redux actions and reducers. All mock data is stored in the global state store. The UI is mainly created based on Material UI from Google.

## Available Scripts

In the project directory, you can run:

### `yarn install`

After clone the repo, please run yarn add to install all dependencies before running the web app.

### `yarn start`

Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `yarn test --watchAll`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn test --silent --watchAll`

There maybe warnings about date format, if you want to ignore them please add --silent
