import actions from './actions';
import * as actionsTypes from './actionTypes';
import moment from 'moment';

const newTask = {
    id: 1,
    subject: 'Create Auth System',
    customer: 'Hungry Jack',
    priority: 'High',
    status: 'Waiting',
    start: moment('Sat Dec 15 2018 23:14:00 GMT+1100').format('MMMM Do YYYY'),
    due: moment('Sat Dec 16 2018 23:14:00 GMT+1100').format('MMMM Do YYYY')
}
const newProject = {
    id: 1,
    name: 'Game Project',
    pstart: moment('Sat Dec 15 2018 23:14:00 GMT+1100'),
    pdue: moment('Sat Dec 16 2018 23:14:00 GMT+1100'),
    customer: 'Jack',
    skills: [
        { key: 0, label: 'Angular' },
        { key: 1, label: 'jQuery' },
        { key: 2, label: 'Polymer' },
        { key: 3, label: 'React' },
        { key: 4, label: 'Vue.js' },
        { key: 5, label: 'Angular' },
        { key: 6, label: 'jQuery' },
    ],
}

describe('Tasks related actions creation test', () => {
    it('should create an action to add a new task', () => {
        const payload = newTask;
        const expectedAction = {
            type: actionsTypes.ADD_NEW_TASK,
            payload: payload
        }
        expect(actions.createNewTaskRequest(payload)).toEqual(expectedAction)
    })
    it('should create an action to delete a task', () => {
        const id = 1;
        const expectedAction = {
            type: actionsTypes.DELETE_TASK,
            payload: id
        }
        expect(actions.deleteTaskRequest(id)).toEqual(expectedAction)
    })
    it('should create an action to edit a task', () => {
        const payload = newTask;
        const expectedAction = {
            type: actionsTypes.EDIT_TASK,
            payload: payload
        }
        expect(actions.editTaskRequest(payload)).toEqual(expectedAction)
    })
})

describe('Projects related actions creation test', () => {
    it('should create an action to add a new project', () => {
        const payload = newProject;
        const expectedAction = {
            type: actionsTypes.ADD_NEW_PROJECT,
            payload: payload
        }
        expect(actions.createNewProjectRequest(payload)).toEqual(expectedAction)
    })
    it('should create an action to delete a project', () => {
        const id = 1;
        const expectedAction = {
            type: actionsTypes.DELETE_PROJECT,
            payload: id
        }
        expect(actions.deleteProjectRequest(id)).toEqual(expectedAction)
    })
    it('should create an action to edit a project', () => {
        const payload = newProject;
        const expectedAction = {
            type: actionsTypes.EDIT_PROJECT,
            payload: payload
        }
        expect(actions.editProjectRequest(payload)).toEqual(expectedAction)
    })
})