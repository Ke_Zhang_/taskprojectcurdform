import {
    ADD_NEW_TASK,
    DELETE_TASK,
    EDIT_TASK,
    ADD_NEW_PROJECT,
    DELETE_PROJECT,
    EDIT_PROJECT,
    ADD_NEW_TASK_RESPONSE
} from "../actionTypes";

const createNewTaskRequest = request => ({
    type: ADD_NEW_TASK,
    payload: request
})

const createNewTaskResponse = response => ({
    type: ADD_NEW_TASK_RESPONSE,
    payload: response
})

const deleteTaskRequest = request => ({
    type: DELETE_TASK,
    payload:request
})

const editTaskRequest = request => ({
    type: EDIT_TASK,
    payload: request
})

const createNewProjectRequest = request => ({
    type: ADD_NEW_PROJECT,
    payload: request
})

const deleteProjectRequest = request => ({
    type: DELETE_PROJECT,
    payload: request
})

const editProjectRequest = request => ({
    type: EDIT_PROJECT,
    payload:request
})

export default {
    createNewTaskRequest,
    deleteTaskRequest,
    editTaskRequest,
    createNewProjectRequest,
    deleteProjectRequest,
    editProjectRequest,
    createNewTaskResponse
}