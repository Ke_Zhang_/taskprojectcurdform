import React, { PureComponent } from 'react';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';

class DialogForm extends PureComponent {
    render() {
        const { deleteId, deleteOpen, handleDeleteClose, handleDelete } = this.props;
        return (
            <Dialog
                open={deleteOpen}
                onClose={()=>{console.log('123')}}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
            >
                <DialogTitle id="alert-dialog-title">{"DELETE TASK"}</DialogTitle>
                <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                        {'Task No. ' + deleteId + ' will be deleted, are you SURE?'}
                    </DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button color="primary" onClick={handleDeleteClose}>
                        No
                    </Button>
                    <Button color="primary" onClick={()=>handleDelete(deleteId)} autoFocus>
                        Yes
                    </Button>
                </DialogActions>
            </Dialog>
        )
    }
}


export default DialogForm;