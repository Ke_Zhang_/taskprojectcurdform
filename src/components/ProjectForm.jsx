import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControl from '@material-ui/core/FormControl';
import { withStyles } from '@material-ui/core/styles';
import { DatePicker } from 'material-ui-pickers';
import FormHelperText from '@material-ui/core/FormHelperText';
import Chip from '@material-ui/core/Chip';
import Fab from '@material-ui/core/Fab';
import AddIcon from '@material-ui/icons/Add';
import Tooltip from '@material-ui/core/Tooltip';

const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120,
        paddingTop: theme.spacing.unit * 3
    },
    fieldControl: {
        margin: theme.spacing.unit,
        minWidth: 700,
    },
    selectEmpty: {
        marginTop: theme.spacing.unit * 2,
    },
    fieldContainer: {
        paddingBottom: theme.spacing.unit * 2
    },
    chip: {
        margin: theme.spacing.unit / 2,
    },
    chipFormControl: {
        margin: theme.spacing.unit,
        minWidth: 120,
        paddingTop: theme.spacing.unit * 3,
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
    },
    skillFieldControl: {
        margin: theme.spacing.unit,
        minWidth: 200,
        paddingTop: theme.spacing.unit * 3,
        display: 'flex',
        flexDirection: 'row'
    },
    fab: {
        margin: theme.spacing.unit,
    }
});

class ProjectForm extends React.Component {

    render() {
        const { classes, title, nameValid,pcustomerValid,skillsValid } = this.props;
        return (
            <div>
                <Dialog
                    open={this.props.open}
                    onClose={this.props.close}
                    aria-labelledby="form-dialog-title"
                    fullWidth={true}
                    maxWidth='md'
                >
                    <DialogTitle id="form-dialog-title">{title}</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            {'Please fill the form to ' + title}
                        </DialogContentText>
                        <div className={classes.fieldContainer}>
                            <FormControl className={classes.fieldControl} error={nameValid} aria-describedby="component-error-text">
                                <TextField
                                    error={nameValid}
                                    autoFocus
                                    margin="dense"
                                    id="name"
                                    label="Name"
                                    fullWidth
                                    inputProps={{ name: 'name' }}
                                    onChange={this.props.handleTextFieldChange}
                                    value={this.props.name}
                                />
                                {
                                    nameValid && <FormHelperText id="component-error-text">Name is empty</FormHelperText>
                                }
                            </FormControl>
                        </div>
                        <div className={classes.fieldContainer}>
                            <FormControl className={classes.fieldControl} error={pcustomerValid} aria-describedby="component-error-text">
                                <TextField
                                    error={pcustomerValid}
                                    margin="dense"
                                    id="pcustomer"
                                    label="Customer"
                                    fullWidth
                                    inputProps={{ name: 'pcustomer' }}
                                    onChange={this.props.handleTextFieldChange}
                                    value={this.props.customer}
                                />
                                {
                                    pcustomerValid && <FormHelperText id="component-error-text">Customer is empty</FormHelperText>
                                }
                            </FormControl>
                        </div>
                        <div className={classes.fieldContainer}>
                            <FormControl className={classes.skillFieldControl} error={skillsValid} aria-describedby="component-error-text">
                                <TextField
                                    error={skillsValid}
                                    margin="dense"
                                    id="skill"
                                    label="Add Skill"
                                    inputProps={{ name: 'skill' }}
                                    onChange={this.props.handleTextFieldChange}
                                    value={this.props.skill}
                                />
                                <Tooltip title="Type skills names and click here to add a skill" placement="right">
                                 <Fab color="primary" aria-label="Add" className={classes.fab} onClick={()=>this.props.handleProjectAddSkills(this.props.skill)}>
                                    <AddIcon />
                                </Fab> 
                                </Tooltip>
                                {
                                    skillsValid && <FormHelperText id="component-error-text">Skill is empty</FormHelperText>
                                }
                            </FormControl>
                        </div>
                        <div className={classes.fieldContainer}>
                            <FormControl className={classes.chipFormControl}>
                                {this.props.skills.map(data => {
                                    let icon = null;
                                    return (
                                        <Chip
                                            key={data.key}
                                            icon={icon}
                                            label={data.label}
                                            onDelete={()=>this.props.handleProjectDeleteSkills(data)}
                                            className={classes.chip}
                                        />
                                    );
                                })}
                            </FormControl>
                        </div>
                        <FormControl className={classes.formControl}>
                            <DatePicker
                                keyboard
                                label="Start Date"
                                format="dd/MM/yyyy"
                                placeholder="10/10/2018"
                                // handle clearing outside => pass plain array if you are not controlling value outside
                                mask={value =>
                                    value ? [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/] : []
                                }
                                value={this.props.selectedStartDate}
                                onChange={this.props.handleStartDateChange}
                                disableOpenOnEnter
                                animateYearScrolling={false}
                            />
                        </FormControl>
                        <FormControl className={classes.formControl}>
                            <DatePicker
                                keyboard
                                label="Due Date"
                                format="dd/MM/yyyy"
                                placeholder="10/10/2018"
                                // handle clearing outside => pass plain array if you are not controlling value outside
                                mask={value =>
                                    value ? [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/] : []
                                }
                                value={this.props.selectedDueDate}
                                onChange={this.props.handleDueDateChange}
                                disableOpenOnEnter
                                animateYearScrolling={false}
                            />
                        </FormControl>

                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.props.close} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.props.handleProjectSubmit} color="primary">
                            Save
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

ProjectForm.propTypes = {
    classes:PropTypes.object.isRequired,
    handleProjectSubmit: PropTypes.func.isRequired,
    close: PropTypes.func.isRequired,
    selectedDueDate: PropTypes.string.isRequired,
    handleStartDateChange: PropTypes.func.isRequired,
    handleDueDateChange: PropTypes.func.isRequired
}

export default withStyles(styles)(ProjectForm);