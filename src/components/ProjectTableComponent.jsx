import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import Tooltip from '@material-ui/core/Tooltip';

const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const styles = theme => ({
  root: {
    width: '80%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
  row: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
  button: {
    margin: theme.spacing.unit,
  },
  iconLayout: {
    display: 'flex',
    flexDirection:'row'
  },
});

function CustomizedTable(props) {
  const { classes } = props;
  console.log(props);
  return (
    <Paper className={classes.root}>
      <Table className={classes.table} >
        <TableHead>
          <TableRow>
            <CustomTableCell>Name</CustomTableCell>
            <CustomTableCell>Time Period</CustomTableCell>
            <CustomTableCell>Customer</CustomTableCell>
            <CustomTableCell>Skills</CustomTableCell>
            <CustomTableCell>Actions</CustomTableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {props.projects.map(row => {
              let a = row.pstart;
              let b = row.pdue;
              let skillsString = '';
              for(let i = 0; i < row.skills.length; i++){
                skillsString = skillsString + row.skills[i].label + ','
              }
            return (
              <TableRow className={classes.row} key={row.id}>
                <CustomTableCell>{row.name}</CustomTableCell>
                <CustomTableCell>{b.diff(a,'d') + ' days'}</CustomTableCell>
                <CustomTableCell>{row.customer}</CustomTableCell>
                <CustomTableCell>{skillsString}</CustomTableCell>
                <CustomTableCell>
                  <div className={classes.iconLayout}>     
                  <Tooltip title="Delete" placement="left">
                    <IconButton className={classes.button}  onClick={()=>props.deleteOpen(row)} aria-label="Delete">
                        <DeleteIcon />
                    </IconButton>
                  </Tooltip>
                 <Tooltip title="Edit" placement="right">
                    <IconButton className={classes.button} onClick={()=>props.editOpen(row)} aria-label="Edit">
                        <EditIcon />
                    </IconButton>
                  </Tooltip>
                  </div>
                </CustomTableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
    </Paper>
  );
}

CustomizedTable.propTypes = {
  classes: PropTypes.object.isRequired,
  projects: PropTypes.array.isRequired,
  deleteOpen: PropTypes.func.isRequired,
  editOpen: PropTypes.func.isRequired
};

export default withStyles(styles)(CustomizedTable);