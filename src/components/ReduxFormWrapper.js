import React from 'react';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import { DatePicker } from 'material-ui-pickers';
import FormControl from '@material-ui/core/FormControl';
import FormHelperText from '@material-ui/core/FormHelperText';
import InputLabel from '@material-ui/core/InputLabel';


const renderTextField = ({
    value,
    className,
    name,
    input,
    label,
    meta: { touched, error },
    ...custom
}) => {
    //console.log(input);
    return (
        <FormControl className={className} error={touched && error} aria-describedby="component-error-text">
            <TextField
                margin="dense"
                label={label}
                {...input}
                {...custom}
            />
            {
                touched && error && <FormHelperText id="component-error-text">required</FormHelperText>
            }
        </FormControl>
    )
}

const renderSelectField = ({
    className,
    input,
    label,
    meta: { touched, error },
    children,
    ...custom
}) => {
    return (
        <FormControl className={className} error={touched && error} aria-describedby="component-error-text">
            <InputLabel htmlFor="status-simple">{label}</InputLabel>
            <Select
                onChange={(event, index, value) => input.onChange(value)}
                children={children}
                {...input}
                {...custom}
            >
            </Select>
            {
                touched && error && <FormHelperText id="component-error-text">required</FormHelperText>
            }
        </FormControl>
    )
}

const renderCalendarField = ({
    className,
    input,
    label,
    meta: { touched, error },
    children,
    ...custom
}) => {
    return (
        <FormControl className={className} aria-describedby="component-error-text">
            <DatePicker
                keyboard
                label={label}
                format="dd/MM/yyyy"
                placeholder="10/10/2018"
                // handle clearing outside => pass plain array if you are not controlling value outside
                mask={value =>
                    value ? [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/] : []
                }
                onChange={(event, index, value) => input.onChange(value)}
                disableOpenOnEnter
                animateYearScrolling={false}
                {...input}
            />
        </FormControl>
    )
}

export {
    renderTextField,
    renderSelectField,
    renderCalendarField
}