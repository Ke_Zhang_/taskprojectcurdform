import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import Tooltip from '@material-ui/core/Tooltip';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
  },
  iconLayout: {
    display: 'flex',
    flexDirection:'row'
  }
});

class TableActionsCell extends PureComponent {
    render() {
        const { classes,deleteId,handelDeleteOpen,data } = this.props;
        return (
            <div className={classes.iconLayout}>
                <Tooltip title="Delete" placement="left">
                    <IconButton className={classes.button} onClick={()=>handelDeleteOpen(deleteId)} aria-label="Delete">
                        <DeleteIcon />
                    </IconButton>
                </Tooltip>
            </div>
        )
    }
}

TableActionsCell.propTypes = {
  classes: PropTypes.object.isRequired,
  //tasks: PropTypes.array.isRequired,
  //deleteOpen: PropTypes.func.isRequired,
  //editOpen: PropTypes.func.isRequired
};

export default withStyles(styles)(TableActionsCell);