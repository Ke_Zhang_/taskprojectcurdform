import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import MenuItem from '@material-ui/core/MenuItem';
import { withStyles } from '@material-ui/core/styles';
import { Field, reduxForm } from 'redux-form'
import {
    renderTextField,
    renderSelectField,
    renderCalendarField
} from './ReduxFormWrapper';


const validate = values => {
    const errors = {}
    const requiredFields = [
        'firstName',
        'lastName',
        'priority'
    ]
    requiredFields.forEach(field => {
        if (!values[field]) {
            errors[field] = 'Required'
        }
    })
    if (
        values.email &&
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
    ) {
        errors.email = 'Invalid email address'
    }
    return errors
}

const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120,
        paddingTop: theme.spacing.unit * 3
    },
    fieldControl: {
        margin: theme.spacing.unit,
        minWidth: 200,
    },
    selectEmpty: {
        marginTop: theme.spacing.unit * 2,
    },
    fieldContainer: {
        paddingBottom: theme.spacing.unit * 2
    },
    formStyle: {
        display: 'flex',
        justifyContent: 'space-between',
        alignItems: 'center'
    }
});

class TableForm extends React.Component {


    render() {
        const { classes, title, open, onClose, handleSubmit, handleTextFieldChange, formTextFields, formSelectFields, formDatePicker } = this.props;
        return (
            <div>
                <Dialog
                    open={open}
                    onClose={onClose}
                    aria-labelledby="form-dialog-title"
                    fullWidth={true}
                    maxWidth='md'
                >
                    <DialogTitle id="form-dialog-title">{title}</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            {'Please fill the form to ' + title}
                        </DialogContentText>
                        <form className={classes.formStyle} onSubmit={handleSubmit}>
                            <div>
                                {
                                    formTextFields.map((value, index) => {
                                        return (
                                            <div key={index}>
                                                <Field
                                                    name={value.name}
                                                    component={renderTextField}
                                                    label={value.label}
                                                    className={classes.fieldControl}
                                                />
                                            </div>
                                        )
                                    })
                                }
                            </div>
                            <div>
                                {
                                    formSelectFields.map((value, index) => {
                                        return (
                                            <div key={index}>
                                                <Field
                                                    name={value.name}
                                                    component={renderSelectField}
                                                    label={value.label}
                                                    className={classes.fieldControl}
                                                >
                                                    {
                                                        value.menuItems.map((v, i) =>
                                                            <MenuItem value={v.value} key={i}>{v.text}</MenuItem>
                                                        )
                                                    }
                                                </Field>
                                            </div>
                                        )
                                    })
                                }
                            </div>
                            <div>
                                {
                                    formDatePicker.map((value, index) => {
                                        return (
                                            <div key={index}>
                                                <Field
                                                    name={value.name}
                                                    component={renderCalendarField}
                                                    label={value.label}
                                                    className={classes.fieldControl}
                                                />
                                            </div>
                                        )
                                    })
                                }
                            </div>
                        </form>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={onClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={handleSubmit} color="primary">
                            Save
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

TableForm.propTypes = {
    classes: PropTypes.object.isRequired,
    //handleSubmit: PropTypes.func.isRequired,
    //close: PropTypes.func.isRequired,
    //selectedDueDate: PropTypes.string.isRequired,
    //handleStartDateChange: PropTypes.func.isRequired,
    //handleDueDateChange: PropTypes.func.isRequired
}

const Form = reduxForm({
    form: 'MaterialUiForm', // a unique identifier for this form
    validate,
})(TableForm)

export default withStyles(styles)(Form);
