import React from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import { withStyles } from '@material-ui/core/styles';
import { DatePicker } from 'material-ui-pickers';
import FormHelperText from '@material-ui/core/FormHelperText';

const styles = theme => ({
    root: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    formControl: {
        margin: theme.spacing.unit,
        minWidth: 120,
        paddingTop: theme.spacing.unit * 3
    },
    fieldControl: {
        margin: theme.spacing.unit,
        minWidth: 700,
    },
    selectEmpty: {
        marginTop: theme.spacing.unit * 2,
    },
    fieldContainer: {
        paddingBottom: theme.spacing.unit * 2
    }
});

class TaskForm extends React.Component {
    render() {
        const { classes, title, subjectValid, customerValid, priorityValid, statusValid } = this.props;
        return (
            <div>
                <Dialog
                    open={this.props.open}
                    onClose={this.props.close}
                    aria-labelledby="form-dialog-title"
                    fullWidth={true}
                    maxWidth='md'
                >
                    <DialogTitle id="form-dialog-title">{title}</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            {'Please fill the form to ' + title}
                        </DialogContentText>
                        <div className={classes.fieldContainer}>
                            <FormControl className={classes.fieldControl} error={subjectValid} aria-describedby="component-error-text">
                                <TextField
                                    error={subjectValid}
                                    autoFocus
                                    margin="dense"
                                    id="subject"
                                    label="Subject"
                                    fullWidth
                                    inputProps={{ name: 'subject' }}
                                    onChange={this.props.handleTextFieldChange}
                                    value={this.props.subject}
                                />
                                {
                                    subjectValid && <FormHelperText id="component-error-text">Subject is empty</FormHelperText>
                                }
                            </FormControl>
                        </div>
                        <div className={classes.fieldContainer}>
                            <FormControl className={classes.fieldControl} error={customerValid} aria-describedby="component-error-text">
                                <TextField
                                    error={customerValid}
                                    margin="dense"
                                    id="customer"
                                    label="Customer"
                                    fullWidth
                                    inputProps={{ name: 'customer' }}
                                    onChange={this.props.handleTextFieldChange}
                                    value={this.props.customer}
                                />
                                {
                                    customerValid && <FormHelperText id="component-error-text">Customer is empty</FormHelperText>
                                }
                            </FormControl>
                        </div>
                        <FormControl className={classes.formControl} error={priorityValid} aria-describedby="component-error-text">
                            <InputLabel htmlFor="priority-simple">Priority</InputLabel>
                            <Select
                                value={this.props.priority}
                                onChange={this.props.changePriority}
                                inputProps={{
                                    name: 'priority',
                                    id: 'priority-simple',
                                }}
                            >
                                <MenuItem value="">
                                    <em>None</em>
                                </MenuItem>
                                <MenuItem value={'Low'}>Low</MenuItem>
                                <MenuItem value={'Mid'}>Mid</MenuItem>
                                <MenuItem value={'High'}>High</MenuItem>
                            </Select>
                            {
                                priorityValid && <FormHelperText id="component-error-text">Priority is empty</FormHelperText>
                            }
                        </FormControl>
                        <FormControl className={classes.formControl} error={statusValid} aria-describedby="component-error-text">
                            <InputLabel htmlFor="status-simple">Status</InputLabel>
                            <Select
                                value={this.props.status}
                                onChange={this.props.changePriority}
                                inputProps={{
                                    name: 'status',
                                    id: 'status-simple',
                                }}
                            >
                                <MenuItem value="">
                                    <em>None</em>
                                </MenuItem>
                                <MenuItem value={'Waiting'}>Waiting</MenuItem>
                                <MenuItem value={'Processing'}>Processing</MenuItem>
                                <MenuItem value={'Complete'}>Complete</MenuItem>
                            </Select>
                            {
                                statusValid && <FormHelperText id="component-error-text">Status is empty</FormHelperText>
                            }
                        </FormControl>
                        <FormControl className={classes.formControl}>
                            <DatePicker
                                keyboard
                                label="Start Date"
                                format="dd/MM/yyyy"
                                placeholder="10/10/2018"
                                // handle clearing outside => pass plain array if you are not controlling value outside
                                mask={value =>
                                    value ? [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/] : []
                                }
                                value={this.props.selectedStartDate}
                                onChange={this.props.handleStartDateChange}
                                disableOpenOnEnter
                                animateYearScrolling={false}
                            />
                        </FormControl>
                        <FormControl className={classes.formControl}>
                            <DatePicker
                                keyboard
                                label="Due Date"
                                format="dd/MM/yyyy"
                                placeholder="10/10/2018"
                                // handle clearing outside => pass plain array if you are not controlling value outside
                                mask={value =>
                                    value ? [/\d/, /\d/, '/', /\d/, /\d/, '/', /\d/, /\d/, /\d/, /\d/] : []
                                }
                                value={this.props.selectedDueDate}
                                onChange={this.props.handleDueDateChange}
                                disableOpenOnEnter
                                animateYearScrolling={false}
                            />
                        </FormControl>

                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.props.close} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.props.handleSubmit} color="primary">
                            Save
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }
}

TaskForm.propTypes = {
    classes:PropTypes.object.isRequired,
    handleSubmit: PropTypes.func.isRequired,
    close: PropTypes.func.isRequired,
    selectedDueDate: PropTypes.string.isRequired,
    handleStartDateChange: PropTypes.func.isRequired,
    handleDueDateChange: PropTypes.func.isRequired
}

export default withStyles(styles)(TaskForm);