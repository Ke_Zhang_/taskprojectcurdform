import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { convertObToArr } from '../helpers';
import TableActionsCell from './TableActionsCell';
import TableForm from './TableForm';
import DialogForm from './Dialog';
import moment from 'moment';

const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

const styles = theme => ({
  root: {
    width: '80%',
    marginTop: theme.spacing.unit * 3,
    overflowX: 'auto',
  },
  table: {
    minWidth: 700,
  },
  row: {
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.background.default,
    },
  },
  button: {
    margin: theme.spacing.unit,
  },
  iconLayout: {
    display: 'flex',
    flexDirection:'row'
  }
});

function CustomizedTable(props) {
  const { handleEditOpen, handleDelete,deleteId,handleDeleteClose, handelDeleteOpen,deleteOpen, handleSubmit,classes, column, data, open, close, onClose, formTextFields, formSelectFields, formDatePicker } = props;
  return (
    <Paper className={classes.root}>
      <Table className={classes.table}>
        <TableHead>
          <TableRow>
          {
            column.map((row,index) => {
              return (
                <CustomTableCell key={index}>{row}</CustomTableCell>
              )
            })
          }
          </TableRow>
        </TableHead>
        <TableBody>
          {data.map(row => {
            return (
              <TableRow className={classes.row} key={row.id}>
                {
                  convertObToArr(row).map((value,index) => {
                    return (
                      <CustomTableCell key={index}>{moment(value).isValid() && index !=0?moment(value).format('MMMM Do YYYY'):value.toString()}</CustomTableCell>
                    )
                  })
                }
                <CustomTableCell><TableActionsCell data={row} deleteId={row.id} handelDeleteOpen={handelDeleteOpen} handleEditOpen={handleEditOpen}/></CustomTableCell>
              </TableRow>
            );
          })}
        </TableBody>
      </Table>
      <TableForm title='add task' handleSubmit={handleSubmit} open={open} onClose={onClose} formTextFields={formTextFields} formSelectFields={formSelectFields} formDatePicker={formDatePicker}/>
      <DialogForm 
          deleteOpen={deleteOpen}
          handleDeleteClose={handleDeleteClose}
          deleteId={deleteId}
          handleDelete={handleDelete}
      />
    </Paper>
  );
}

CustomizedTable.propTypes = {
  classes: PropTypes.object.isRequired,
  //tasks: PropTypes.array.isRequired,
  //deleteOpen: PropTypes.func.isRequired,
  //editOpen: PropTypes.func.isRequired
};

export default withStyles(styles)(CustomizedTable);