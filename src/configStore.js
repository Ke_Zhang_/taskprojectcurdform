import { createStore, applyMiddleware, compose, combineReducers } from 'redux';
import logger from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import { reducer as formReducer } from 'redux-form';
import {
    taskReducer,
    projectReducer,
    //FormReducer
} from './reducers';
import rootSaga from './sagas';

export default function configStore(initialState = {}) {

    const reducers = {
        taskReducer: taskReducer,
        projectReducer: projectReducer,
        form: formReducer
    };
    const sagaMiddleware = createSagaMiddleware();
    const rootReducer = combineReducers(reducers)
    const store = createStore(
        rootReducer,
        initialState,
        compose(
            applyMiddleware(
                sagaMiddleware,
                logger
            )
        )
    );
    sagaMiddleware.run(rootSaga);
    if (module.hot) {
        module.hot.accept(() => {
            const nextRootReducer = rootReducer;
            store.replaceReducer(nextRootReducer);
        });
    }

    return store;
}