import React, { Component } from 'react';
import '../App.css';
import TaskTableComponent from '../components/TaskTableComponent';
import ProjectTableComponent from '../components/ProjectTableComponent';
import TableComponent from '../components/TableComponent';
// import DialogForm from '../components/Dialog';
import MySnackbarContentWrapper from '../components/MySnackbarContentWrapper';
import TaskForm from '../components/TaskForm';
import ProjectForm from '../components/ProjectForm';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from '../actions';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import Snackbar from '@material-ui/core/Snackbar';

import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import { withStyles } from '@material-ui/core/styles';
import AddIcon from '@material-ui/icons/Add';
import Button from '@material-ui/core/Button';

import { MuiPickersUtilsProvider } from 'material-ui-pickers';
import DateFnsUtils from '@date-io/date-fns';
import moment from 'moment';
import uuidv1 from 'uuid/v1';

const columnTaskForm = [
    'Task No.',
    'Subject',
    'Customer',
    'Priority',
    'Status',
    'Start Date',
    'Due Date',
    'Actions'
]

function TableTaskComponent(props) {
    return (
        <div className='container'>
            <TaskTableComponent
                column={columnTaskForm}
                data={props.tasks}
                open={props.open}
                onClose={props.onClose}
                formTextFields={[{ name: 'subject', label: 'Subject' }, { name: 'customer', label: 'Customer' }]}
                formSelectFields={[
                    { menuItems: [{ value: 'low', text: 'Low' }, { value: 'mid', text: 'Mid' }, { value: 'high', text: 'High' }], name: 'priority', label: 'Priority' },
                    { menuItems: [{ value: 'Waiting', text: 'Waiting' }, { value: 'Processing', text: 'Processing' }, { value: 'Completed', text: 'Completed' }], name: 'status', label: 'Status' }
                ]}
                formDatePicker={[
                    { name: 'start', label: 'Start Date' },
                    { name: 'due', label: 'Due Date' }
                ]}
                handleSubmit={props.handleSubmit}
                deleteOpen={props.deleteOpen}
                handelDeleteOpen={props.handelDeleteOpen}
                handleDeleteClose={props.handleDeleteClose}
                deleteId={props.deleteId}
                handleDelete={props.handleDelete}
                handleEditOpen={props.handleEditOpen}
            />
            <div className='fabIconContainer'>
                <Button onClick={props.onOpen} variant="contained" color="secondary" fullWidth={true} className={props.customStyle.button}>
                    Add New Task (mimic saga)
                    <AddIcon className={props.customStyle.rightIcon} />
                </Button>
            </div>
        </div>
    );
}

function TableProjectComponent(props) {
    return (
        <div className='container'>
            <ProjectTableComponent
                tableName="Projects"
                projects={props.projects}
                deleteOpen={props.deleteOpen}
                editOpen={props.editOpen}
            />
            <div className='fabIconContainer'>
                <Button onClick={props.click} variant="contained" color="secondary" fullWidth={true} className={props.customStyle.button}>
                    Add New Project
                    <AddIcon className={props.customStyle.rightIcon} />
                </Button>
            </div>
        </div>
    );
}

const styles = theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: theme.palette.background.paper,
    },
    button: {
        margin: theme.spacing.unit,
    },
    rightIcon: {
        marginLeft: theme.spacing.unit,
    },
});

class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            //snackbar
            snackbarOpen: false,
            snackBarMessage: '',
            snackBarVariant: '',
            // project form
            projectDeleteOpen: false,
            projectDeleteId: 0,
            projectDeleteName: '',
            projectOpen: false,
            projectFormType: 'add',
            ptitle: 'Add New Project',
            // task form
            formType: 'add',
            title: 'Add New Task',
            value: 0,
            open: false,
            deleteOpen: false,
            deleteId: 0,
            subjectValid: false,
            customerValid: false,
            priorityValid: false,
            statusValid: false,
            selectedStartDateValid: false,
            selectedDueDateValid: false,
            //project form validation
            nameValid: false,
            pcustomerValid: false,
            skillsValid: false,
            // task form data
            id: 0,
            subject: '',
            customer: '',
            priority: '',
            status: '',
            selectedStartDate: new Date(),
            selectedDueDate: new Date(),
            //project form data
            pid: 0,
            name: '',
            pstart: new Date(),
            pdue: new Date(),
            pcustomer: '',
            skills: [],
            skill: '',
            valueForm: []
        };
    }

    handleSubmitForm = () => {
        this.props.createNewTaskRequest({
            id: this.props.tasks.length > 0 ? this.props.tasks[this.props.tasks.length - 1].id + 1 : 1,
            ...this.props.formData.values
        })
        this.setState({ open: false, snackbarOpen: true, snackBarMessage: 'Successfully add new task!', snackBarVariant: 'success' });
    }

    handleChangeForm = (event, value) => {
        this.setState({ value });
    };
    handleSnackBarClose = () => {
        this.setState({ snackbarOpen: false })
    };
    //project event handler
    handleProjectEditOpen = (row) => {
        this.setState({ projectOpen: true, formType: 'edit', ptitle: 'Edit Project ' + row.name, pid: row.id, name: row.name, pcustomer: row.customer, skills: row.skills, skill: '', pstart: row.pstart, pdue: row.pdue });
    };
    handleProjectDeleteOpen = (project) => {
        this.setState({ projectDeleteOpen: true, projectDeleteId: project.id, projectDeleteName: project.name });
    };
    handleProjectDeleteClose = () => {
        this.setState({ projectDeleteOpen: false, projectDeleteId: 0 })
    };
    handleProjectDelete = (id) => {
        this.props.deleteProjectRequest(id);
        this.setState({ projectDeleteOpen: false, projectDeleteId: 0, snackbarOpen: true, snackBarMessage: 'Successfully delete a project!', snackBarVariant: 'info' });
    }
    handleProjectClickOpen = () => {
        this.setState({ projectOpen: true, title: 'Add New Project', formType: 'add', name: '', pcustomer: '', skills: [], skill: '' });
    };
    handleProjectClose = () => {
        this.setState({ projectOpen: false });
    };
    handleProjectTextFieldChange = event => {
        this.setState({ [event.target.name]: event.target.value, nameValid: false, pcustomerValid: false });
    };
    handleProjectStartDateChange = date => {
        this.setState({ pstart: date });
    };
    handleProjectDueDateChange = date => {
        this.setState({ pdue: date });
    };
    handleProjectAddSkills = data => {
        if (data !== '') {
            let newSkill = {
                id: uuidv1(),
                label: data
            }
            let skillSets = [...this.state.skills];
            skillSets.push(newSkill);
            this.setState({ skills: skillSets, skill: '', skillsValid: false });
        }
    };
    handleProjectDeleteSkills = data => {
        this.setState(state => {
            const skills = [...state.skills];
            const chipToDelete = skills.indexOf(data);
            skills.splice(chipToDelete, 1);
            return { skills };
        });
    };
    handleProjectSubmit = () => {
        let pass = true;
        const { pid, name, pcustomer, skills, pstart, pdue, formType } = this.state;
        if (name === '') {
            this.setState({ nameValid: true })
            pass = false;
        } else {
            this.setState({ nameValid: false })
        }
        if (pcustomer === '') {
            this.setState({ pcustomerValid: true })
            pass = false;
        } else {
            this.setState({ pcustomerValid: false })
        }
        if (skills.length === 0) {
            this.setState({ skillsValid: true })
            pass = false;
        } else {
            this.setState({ skillsValid: false })
        }

        if (pass) {
            if (formType === 'add') {
                this.props.createNewProjectRequest({
                    id: uuidv1(),
                    name: name,
                    pstart: moment(pstart),
                    pdue: moment(pdue),
                    customer: pcustomer,
                    skills: skills
                })
                this.setState({ projectOpen: false, snackbarOpen: true, snackBarMessage: 'Successfully add new project!', snackBarVariant: 'success' });
            } else {
                this.props.editProjectRequest({
                    id: pid,
                    name: name,
                    pstart: moment(pstart),
                    pdue: moment(pdue),
                    customer: pcustomer,
                    skills: skills
                })
                this.setState({ projectOpen: false, snackbarOpen: true, snackBarMessage: 'Successfully update a project!', snackBarVariant: 'info' });
            }
        }

    };
    // task event handler
    handleClickOpen = () => {
        this.setState({ open: true, title: 'Add Task', formType: 'add', subject: '', customer: '', priority: '', status: '', selectedStartDate: new Date(), selectedDueDate: new Date() });
    };
    handleDeleteOpen = (id) => {
        this.setState({ deleteOpen: true, deleteId: id });
    };
    handleDeleteClose = () => {
        this.setState({ deleteOpen: false, deleteId: 0 })
    };
    handleDelete = (id) => {
        this.props.deleteTaskRequest(id);
        this.setState({ deleteOpen: false, deleteId: 0, snackbarOpen: true, snackBarMessage: 'Successfully delete a task!', snackBarVariant: 'info' });
    }
    handleEditOpen = (row) => {
        this.setState({ open: true, formType: 'edit', title: 'Edit Task No.' + row.id, id: row.id, subject: row.subject, customer: row.customer, priority: row.priority, status: row.status, selectedStartDate: new Date(moment.utc(row.start, 'MMMM Do YYYY').format()), selectedDueDate: new Date(moment.utc(row.due, 'MMMM Do YYYY').format()) });
    }
    handleClose = () => {
        this.setState({ open: false });
    };
    handleTextFieldChange = event => {
        this.setState({ [event.target.name]: event.target.value, subjectValid: false, customerValid: false });
    };
    handlePriorityChange = event => {

        this.setState({ [event.target.name]: event.target.value, priorityValid: false, statusValid: false });
    };
    handleStartDateChange = date => {
        this.setState({ selectedStartDate: date });
    };
    handleDueDateChange = date => {
        this.setState({ selectedDueDate: date });
    };
    handleSubmit = () => {
        let pass = true;
        const { id, subject, customer, priority, status, selectedStartDate, selectedDueDate, formType } = this.state;
        if (subject === '') {
            this.setState({ subjectValid: true })
            pass = false;
        } else {
            this.setState({ subjectValid: false })
        }
        if (customer === '') {
            this.setState({ customerValid: true })
            pass = false;
        } else {
            this.setState({ customerValid: false })
        }
        if (priority === '') {
            this.setState({ priorityValid: true })
            pass = false;
        } else {
            this.setState({ priorityValid: false })
        }
        if (status === '') {
            this.setState({ statusValid: true })
            pass = false;
        } else {
            this.setState({ statusValid: false })
        }
        if (pass) {
            let len = this.props.tasks.length;
            if (formType === 'add') {
                this.props.createNewTaskRequest({
                    id: len > 0 ? this.props.tasks[len - 1].id + 1 : 1,
                    subject,
                    customer,
                    priority,
                    status,
                    start: moment(selectedStartDate).format('MMMM Do YYYY'),
                    due: moment(selectedDueDate).format('MMMM Do YYYY'),
                })
                this.setState({ open: false, snackbarOpen: true, snackBarMessage: 'Successfully add new task!', snackBarVariant: 'success' });
            } else {
                this.props.editTaskRequest({
                    id: id,
                    subject,
                    customer,
                    priority,
                    status,
                    start: moment(selectedStartDate).format('MMMM Do YYYY'),
                    due: moment(selectedDueDate).format('MMMM Do YYYY'),
                })
                this.setState({ open: false, snackbarOpen: true, snackBarMessage: 'Successfully update task!', snackBarVariant: 'info' });
            }
        }
    }

    render() {
        const { classes, tasks, projects } = this.props;
        const { value } = this.state;
        return (
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <div className={classes.root}>
                    <AppBar position="static">
                        <Tabs value={value} onChange={this.handleChangeForm}>
                            <Tab label="Form with Redux-Form" />
                            <Tab label="Normal Form" />
                        </Tabs>
                    </AppBar>
                    {value === 0 &&
                        <TableTaskComponent
                            open={this.state.open}
                            onOpen={this.handleClickOpen}
                            onClose={this.handleClose}
                            customStyle={classes}
                            tasks={tasks}
                            deleteOpen={this.state.deleteOpen}
                            deleteId={this.state.deleteId}
                            handelDeleteOpen={this.handleDeleteOpen}
                            handleDeleteClose={this.handleDeleteClose}
                            handleEditOpen={this.handleEditOpen}
                            data={this.props.formData}
                            handleSubmit={this.handleSubmitForm}
                            handleDelete={this.handleDelete}
                        />}
                    {value === 1 && <TableProjectComponent customStyle={classes} click={this.handleProjectClickOpen} projects={projects} deleteOpen={this.handleProjectDeleteOpen} editOpen={this.handleProjectEditOpen} />}
                    <ProjectForm
                        title={this.state.ptitle}
                        open={this.state.projectOpen}
                        close={this.handleProjectClose}
                        name={this.state.name}
                        customer={this.state.pcustomer}
                        selectedStartDate={this.state.pstart}
                        selectedDueDate={this.state.pdue}
                        skills={this.state.skills}
                        skill={this.state.skill}
                        handleTextFieldChange={this.handleProjectTextFieldChange}
                        handleStartDateChange={this.handleProjectStartDateChange}
                        handleDueDateChange={this.handleProjectDueDateChange}
                        handleProjectAddSkills={this.handleProjectAddSkills}
                        handleProjectDeleteSkills={this.handleProjectDeleteSkills}
                        handleProjectSubmit={this.handleProjectSubmit}
                        //validation
                        nameValid={this.state.nameValid}
                        pcustomerValid={this.state.pcustomerValid}
                        skillsValid={this.state.skillsValid}
                    />
                    <Dialog
                        open={this.state.projectDeleteOpen}
                        onClose={this.handleProjectDeleteClose}
                        aria-labelledby="alert-dialog-title"
                        aria-describedby="alert-dialog-description"
                    >
                        <DialogTitle id="alert-dialog-title">{"DELETE PROJECT"}</DialogTitle>
                        <DialogContent>
                            <DialogContentText id="alert-dialog-description">
                                {'Project ' + this.state.projectDeleteName + ' will be deleted, are you SURE?'}
                            </DialogContentText>
                        </DialogContent>
                        <DialogActions>
                            <Button color="primary" onClick={this.handleProjectDeleteClose}>
                                No
                            </Button>
                            <Button color="primary" autoFocus onClick={() => this.handleProjectDelete(this.state.projectDeleteId)}>
                                Yes
                            </Button>
                        </DialogActions>
                    </Dialog>
                    <Snackbar
                        anchorOrigin={{
                            vertical: 'bottom',
                            horizontal: 'left',
                        }}
                        open={this.state.snackbarOpen}
                        autoHideDuration={2000}
                        onClose={this.handleSnackBarClose}
                    >
                        <MySnackbarContentWrapper
                            onClose={this.handleSnackBarClose}
                            variant={this.state.snackBarVariant}
                            message={this.state.snackBarMessage}
                        />
                    </Snackbar>
                </div>
            </MuiPickersUtilsProvider>
        );
    }
}

function mapStateToProps(state) {
    return {
        tasks: state.taskReducer.rowData,
        projects: state.projectReducer.rowData,
        formData: state.form.MaterialUiForm
    };
}

function mapDispatchToProps(dispatch) {
    return bindActionCreators({ ...actions }, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(withStyles(styles)(App));
