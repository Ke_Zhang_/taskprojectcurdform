const convertObToArr = obj => {
    let array = [];
    Object.keys(obj).forEach((key,index) => {
        array[index] = obj[key];
    })
    return array;
}

export {
    convertObToArr
}