import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './containers/dashboardContainer';
import * as serviceWorker from './serviceWorker';

import configStore from './configStore';
import { Provider } from 'react-redux';

ReactDOM.render(<Provider store={configStore()}><App /></Provider>, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
