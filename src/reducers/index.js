
import {
    ADD_NEW_TASK,
    DELETE_TASK,
    EDIT_TASK,
    ADD_NEW_PROJECT,
    DELETE_PROJECT,
    EDIT_PROJECT,
    ADD_NEW_TASK_RESPONSE
} from "../actionTypes";
import moment from 'moment';
//{ id, subject, customer, priority, status, start, due};
const initialTaskTableData = [
    {
        id:1,
        subject:'Create Auth System',
        customer: 'Hungry Jack',
        priority: 'High',
        status: 'Waiting',
        start: moment('Sat Dec 15 2018 23:14:00 GMT+1100').format('MMMM Do YYYY'),
        due: moment('Sat Dec 16 2018 23:14:00 GMT+1100').format('MMMM Do YYYY')
        //start:'Sat Dec 22 2018 00:57:00 GMT+1100 (Australian Eastern Daylight Time)',
        //due:'Sat Dec 25 2018 00:57:00 GMT+1100 (Australian Eastern Daylight Time)'
    }
]

const initialProjectTableData = [
    {
        id:1,
        name:'Game Project',
        pstart:moment('Sat Dec 15 2018 23:14:00 GMT+1100'),
        pdue: moment('Sat Dec 16 2018 23:14:00 GMT+1100'),
        customer:'Jack',
        skills:[
            { key: 0, label: 'Angular' },
            { key: 1, label: 'jQuery' },
            { key: 2, label: 'Polymer' },
            { key: 3, label: 'React' },
            { key: 4, label: 'Vue.js' },
            { key: 5, label: 'Angular' },
            { key: 6, label: 'jQuery' },
        ],
    },
    {
        id:2,
        name:'Shop Project',
        pstart:moment('Sat Dec 15 2018 23:14:00 GMT+1100'),
        pdue: moment('Sat Dec 29 2018 23:14:00 GMT+1100'),
        customer:'Jim',
        skills:[
            { key: 0, label: 'Angular' },
            { key: 1, label: 'jQuery' },
            { key: 2, label: 'Polymer' },
            { key: 3, label: 'React' },
        ],
    },
    {
        id:3,
        name:'Train Project',
        pstart:moment('Sat Dec 15 2018 23:14:00 GMT+1100'),
        pdue: moment('Sat Dec 29 2018 23:14:00 GMT+1100'),
        customer:'Google',
        skills:[
            { key: 0, label: 'Angular' },
            { key: 1, label: 'jQuery' },
            { key: 2, label: 'Polymer' },
            { key: 3, label: 'React' },
        ],
    },
    {
        id:4,
        name:'Road Project',
        pstart:moment('Sat Dec 15 2018 23:14:00 GMT+1100'),
        pdue: moment('Sat Dec 29 2018 23:14:00 GMT+1100'),
        customer:'Apple Pty Ltd',
        skills:[
            { key: 0, label: 'Angular' },
            { key: 1, label: 'jQuery' },
            { key: 2, label: 'Polymer' },
            { key: 3, label: 'React' },
        ],
    }
]
const taskReducer = (state={rowData:initialTaskTableData}, action) => {
    switch(action.type){
        // case ADD_NEW_TASK:
        // let newRowData = [...state.rowData];
        // newRowData.push(action.payload);
        // return {
        //     rowData: newRowData
        // }
        case ADD_NEW_TASK_RESPONSE:
        let newRowData = [...state.rowData];
        newRowData.push(action.payload);
        return {
            rowData: newRowData
        }
        case DELETE_TASK:
        let deleteRowData = [...state.rowData];
        for(let i = 0; i < state.rowData.length; i++){
            if(state.rowData[i].id === action.payload){
                deleteRowData.splice(i, 1);
            }
        }
        return {
            rowData: deleteRowData
        }
        case EDIT_TASK:
        let editRowData = [...state.rowData];
        for(let i = 0; i < state.rowData.length; i++){
            if(state.rowData[i].id === action.payload.id){
                editRowData[i] = action.payload;
            }
        }
        return {
            rowData: editRowData
        }
        default:
        return state;
    }
}

const projectReducer = (state = {rowData:initialProjectTableData}, action) => {
    switch(action.type){
        case ADD_NEW_PROJECT:
        let newRowData = [...state.rowData];
        newRowData.push(action.payload);
        return {
            rowData: newRowData
        }
        case DELETE_PROJECT:
        let deleteRowData = [...state.rowData];
        for(let i = 0; i < state.rowData.length; i++){
            if(state.rowData[i].id === action.payload){
                deleteRowData.splice(i, 1);
            }
        }
        return {
            rowData: deleteRowData
        }
        case EDIT_PROJECT:
        let editRowData = [...state.rowData];
        for(let i = 0; i < state.rowData.length; i++){
            if(state.rowData[i].id === action.payload.id){
                editRowData[i] = action.payload;
            }
        }
        return {
            rowData: editRowData
        }
        default:
        return state;
    }
}



export {
    taskReducer,
    projectReducer,
    initialTaskTableData,
    initialProjectTableData
}
