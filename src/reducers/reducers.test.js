import actions from './actions';
import * as actionsTypes from './actionTypes';
import * as reducers from './reducers';
import moment from 'moment';
import uuidv1 from 'uuid/v1';

let testData = {
    id: 2,
    subject: 'Create Auth System',
    customer: 'Hungry Jack',
    priority: 'High',
    status: 'Waiting',
    start: moment('Sat Dec 15 2018 23:14:00 GMT+1100').format('MMMM Do YYYY'),
    due: moment('Sat Dec 16 2018 23:14:00 GMT+1100').format('MMMM Do YYYY')
}
let addData = {
    rowData: [
        { ...reducers.initialTaskTableData[0] }
    ]
}
addData.rowData.push(testData);

let testProjectData = {
    id: 10,
    name: 'Game Project',
    pstart: moment('Sat Dec 15 2018 23:14:00 GMT+1100'),
    pdue: moment('Sat Dec 16 2018 23:14:00 GMT+1100'),
    customer: 'Jack',
    skills: [
        { key: 0, label: 'Angular' },
        { key: 1, label: 'jQuery' },
        { key: 2, label: 'Polymer' },
        { key: 3, label: 'React' },
        { key: 4, label: 'Vue.js' },
        { key: 5, label: 'Angular' },
        { key: 6, label: 'jQuery' },
    ],
}

let addProjectData = {
    rowData: [...reducers.initialProjectTableData]
}
addProjectData.rowData.push(testProjectData);


    describe('taskReducer', () => {

        it('should return the initial state of taskReducer', () => {
            expect(reducers.taskReducer({ rowData: reducers.initialTaskTableData }, {})).toEqual({ rowData: reducers.initialTaskTableData })
        })

        it('should handle ADD_NEW_TASK action', () => {
            expect(reducers.taskReducer(
                { rowData: reducers.initialTaskTableData },
                {
                    type: actionsTypes.ADD_NEW_TASK,
                    payload: testData
                }
            )).toEqual(addData)
        })

        it('should handle DELETE_TASK action', () => {
            expect(reducers.taskReducer(
                { rowData: addData.rowData },
                {
                    type: actionsTypes.DELETE_TASK,
                    payload: 2
                }
            )).toEqual({ rowData: reducers.initialTaskTableData })
        })

        it('should handle EDIT_TASK action', () => {
            expect(reducers.taskReducer(
                { rowData: reducers.initialTaskTableData },
                {
                    type: actionsTypes.EDIT_TASK,
                    payload: reducers.initialTaskTableData[0]
                }
            )).toEqual({ rowData: reducers.initialTaskTableData })
        })
    })

describe('projectReducer', () => {
    it('should return the initial state of projectReducer', () => {
        expect(reducers.projectReducer({ rowData: reducers.initialProjectTableData }, {})).toEqual({ rowData: reducers.initialProjectTableData })
    })
    it('should handle ADD_NEW_PROJECT action', () => {
        expect(reducers.projectReducer(
            { rowData: reducers.initialProjectTableData },
            {
                type: actionsTypes.ADD_NEW_PROJECT,
                payload: testProjectData
            }
        )).toEqual(addProjectData)
    })

    it('should handle DELETE_PROJECT action', () => {
        expect(reducers.projectReducer(
            {rowData: addProjectData.rowData},
            {
                type: actionsTypes.DELETE_PROJECT,
                payload: 10
            }
        )).toEqual({ rowData: reducers.initialProjectTableData })
    })

    it('should handle EDIT_PROJECT action', () => {
        expect(reducers.projectReducer(
            {rowData: reducers.initialProjectTableData},
            {
                payload: actionsTypes.EDIT_PROJECT,
                payload: reducers.initialProjectTableData[0]
            }
        )).toEqual({ rowData: reducers.initialProjectTableData })
    })
})