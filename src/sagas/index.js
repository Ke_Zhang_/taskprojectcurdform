import {
    fork,
    call,
    put,
    take,
} from 'redux-saga/effects';
import * as actionTypes from '../actionTypes';
import actions from '../actions';



function* addNewTaskRequest() {
    while (true) {
        const addNewTaskRequest = yield take(actionTypes.ADD_NEW_TASK);
        const post = yield call(mimicPost); // raw data from the response
        yield put(actions.createNewTaskResponse(addNewTaskRequest.payload));
        // response from server may still include the raw data, and a 200 code to indicate status.
    }
}

function mimicPost() {
    return new Promise(function (resolve, reject) {
        // do something with server activities, for example, post a new data and waiting for response
        setTimeout(resolve, 1000);
    });
}

export default function* rootSaga() {
    yield [
        fork(addNewTaskRequest),
    ];
}